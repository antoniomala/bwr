# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    
    # home do site, será a unica parte do site ao usuário externo
	url(r'^$', 'apps.website.views.home_page', name='home'),

	# url Team
	url(r'^administracao/$', 'apps.administracao.views.logar_usuario'),
	url(r'^sair/$', 'apps.administracao.views.sair'),


	url(r'^administracao/listar_team/$', 'apps.administracao.views.listar_team', name='listar_team'),
	url(r'^administracao/team_novo/$', 'apps.administracao.views.team_novo', name='team_novo'),
	url(r'^administracao/team_edit/(?P<id>\d+)/$', 'apps.administracao.views.team_edit', name='team_edit'),
	url(r'^administracao/team_excluir/(?P<id>\d+)/$', 'apps.administracao.views.team_excluir', name='team_excluir'),

	url(r'^administracao/listar_work/$', 'apps.administracao.views.listar_work', name='listar_work'),
	url(r'^administracao/work_novo/$', 'apps.administracao.views.work_novo', name='work_novo'),
	url(r'^administracao/work_edit/(?P<id>\d+)/$', 'apps.administracao.views.work_edit', name='work_edit'),
	url(r'^administracao/work_excluir/(?P<id>\d+)/$', 'apps.administracao.views.work_excluir', name='work_excluir'),

	url(r'^media/(.*)/$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)