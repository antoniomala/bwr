# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.forms import ModelForm

# Create your models here.
class Work(models.Model):
	nome = models.CharField(u'nome', max_length=20)
	categoria = models.CharField(u'Categoria', max_length=20, 
		choices=(('photography', 'Imagens'), ('video', 'Videos'), ('design', 'design')), default='photography')
	foto = models.FileField(u'foto', upload_to=settings.MEDIA_ROOT)
	descricao = models.TextField(u'descrição')
	video = models.URLField('link do vídeo caso seja do tipo vídeo', null=True, blank=True, default='')

	def __unicode__(self):
		return self.nome

class WorkForm(ModelForm):
	class Meta:
		model = Work
		exclude = []

# área para as signals
# ajustando o campo foto no model Trabalhos para que este não capture todo o endereço da foto

def nome_foto(sender, instance, signal, *args, **kwargs):
	# checando se a instância possui os atributos image e id
	if instance.id and hasattr(instance, 'foto') and not hasattr(instance, '_already_save'):
		meu_local = str(instance.foto).split('/').pop()
		instance.foto = meu_local
		instance._already_save = True
		instance.save()

# chamada ao signal post_save
post_save.connect(nome_foto, sender=Work)