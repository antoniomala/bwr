# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from apps.team.models import *
from apps.galeria.models import *


def home_page(request):
	team = Team.objects.all()
	works = Work.objects.all()
	return render(request, 'index.html', {'works': works, 'team': team})