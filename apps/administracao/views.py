# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponseRedirect
from apps.team.models import *
from apps.galeria.models import *
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import authenticate, login, logout

# login no sistema
def logar_usuario(request):
	# checando se o usuário já realizou o login e tomanda a decisão adequada
	if request.user.is_authenticated():
		if request.user.is_superuser:
			return HttpResponseRedirect('/administracao/listar_team/')
		else:
			return HttpResponseRedirect('/')


	form = AuthenticationForm(request.POST or None)
	erro = False

	if request.method == 'POST':
		usuario = authenticate(username=request.POST['username'], password=request.POST['password'])

		if usuario is not None:
			login(request, usuario)
			
			# redirecionar para local adequado
			if usuario.is_superuser:
				return HttpResponseRedirect('/administracao/')
			else:
				# redireciona para área do cliente
				return HttpResponseRedirect('/cliente/')

		else: erro = True
	
	return render(request, 'login.html', {'form': form, 'erro': erro})

# logout no sistema
def sair(request):
	logout(request)
	return HttpResponseRedirect('/')

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def listar_team(request):
	team = Team.objects.all()
	return render(request, 'team/listar_team.html', {'team': team})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def team_novo(request):
	form = TeamForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()
		return HttpResponseRedirect('/administracao/listar_team/')

	return render(request, 'team/team_novo.html', {'form': form})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def team_edit(request, id):
	instancia = Team.objects.get(pk=id)

	form = TeamForm(request.POST or None, request.FILES or None, instance=instancia)

	if form.is_valid():
		form.save()
		return HttpResponseRedirect('/administracao/listar_team/')

	return render(request, 'team/team_edit.html', {'form': form})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def team_excluir(request, id):
	team = Team.objects.get(pk=id)
	team.delete()
	return HttpResponseRedirect('/administracao/listar_team/')


@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def listar_work(request):
	work = Work.objects.all()
	return render(request, 'work/listar_work.html', {'work': work})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def work_novo(request):
	form = WorkForm(request.POST or None, request.FILES or None)

	if form.is_valid():
		form.save()
		return HttpResponseRedirect('/administracao/listar_work/')

	return render(request, 'work/work_novo.html', {'form': form})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def work_edit(request, id):
	instancia = Work.objects.get(pk=id)

	form = WorkForm(request.POST or None, request.FILES or None, instance=instancia)

	if form.is_valid():
		form.save()
		return HttpResponseRedirect('/administracao/listar_work/')

	return render(request, 'work/work_edit.html', {'form': form})

@user_passes_test(lambda u: u.is_superuser, login_url='/admin')
def work_excluir(request, id):
	work = Work.objects.get(pk=id)
	work.delete()
	return HttpResponseRedirect('/administracao/listar_work/')